#!/usr/bin/env bash

sed -i 's/101/0/g' /usr/sbin/policy-rc.d
apt-get update
apt-get install -y software-properties-common
add-apt-repository ppa:ondrej/php -y && \
apt-get update

apt-get install -y \
     php7.1-fpm \
     php7.1-curl \
     php7.1-gd \
     php7.1-mcrypt \
     php7.1-mysql \
     php7.1-xsl \
     php-xdebug \
     php7.1-memcache \
     php7.1-redis \
     php7.1-imagick \
     php7.1-dev \
     pkg-config \
     php7.1-cli \
     php-pear \
     php7.1-common \
     php7.1-bz2\
     php7.1-mbstring\
     php7.1-zip \
     php7.1-sqlite3

/usr/bin/pecl install mongodb

pear install PHP_CodeSniffer

echo "extension=mongodb.so" > /etc/php/7.1/mods-available/mongodb.ini
ln -s /etc/php/7.1/mods-available/mongodb.ini /etc/php/7.1/fpm/conf.d/20-mongodb.ini
ln -s /etc/php/7.1/mods-available/mongodb.ini /etc/php/7.1/cli/conf.d/20-mongodb.ini

sed -i 's/listen = \/run\/php\/php7.1-fpm.sock/listen = 9000/g' /etc/php/7.1/fpm/pool.d/www.conf
sed -i \
    -e '/^display_errors =/c\display_errors = On' \
    -e '/^display_startup_errors =/c\display_startup_errors = On' \
    -e '/^;date.timezone =/c\date.timezone = UTC' \
    -e '/^upload_max_filesize =/c\upload_max_filesize = 8m' \
    -e '/^error_reporting =/c\error_reporting = E_ALL' /etc/php/7.1/fpm/php.ini /etc/php/7.1/cli/php.ini

rm /etc/php/7.1/mods-available/opcache.ini
apt-get install wkhtmltopdf

apt-get autoclean
apt-get clean
du -sh /var/cache/apt/archives
apt-get autoremove

